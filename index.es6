var Promise = require("bluebird");
var extend = require("extend");

//console.log('FACTORY LOADED!');

// this object must have a run function
// this program enriches factory.widget
// with methods and ui runnable actions

class Control {

  constructor({ widget, info, card, flow, code }) {

    this.widget = widget;
    this.info = info;
    this.card = card;
    this.flow = flow;
    this.code = code;

    // Persistent memory on per-control basis.
    this.data = {widget: this.widget};
  }

  run({ action } = { action: 'boot' }) {

    if(!action) {
      console.warn(`Warning: ${this.info.name}.${this.card.name} tried to execute an action but dailed to specify name.`);
      return;
    }

    if( (!this.flow.data) || (!this.flow.data[action]) ) {
      console.warn(`Warning: ${this.info.name}.${this.card.name} does not recognize action named ${action}. Please check flow.json and make sure it includes data.${action} object. Action chain cancelled.`);
      return;
    }

    if( (!this.flow.data[action].data) || (this.flow.data[action].data.length === 0) ){
      // NOTE: don't remove this, this is important information.
      console.warn(`Info: ${this.info.name}.${this.card.name} has an empty ${action} flow.`);
      return;
    }




    let resolver = ({action, resolved=[]}) => {

      let tasks = this.flow.data[action].data;

      tasks.forEach((task) => {
        let gosub = false;

        // What makes a gosub? The existence of that same name in the flow as a category.
        if( this.flow.data[task.id]  ) gosub = true;

        if(gosub){
          // Do not push the task if it is a gosub, push contents of gosub.
          resolver({action:task.id, resolved});
        } else {
          resolved.push(task);
        }

      });

      return resolved;
    }

    let resolved = resolver({action});

     //console.log(`Executing ${this.info.name}.${this.card.name}.${action}`);
     //console.log(`Executing ${this.info.name}.${this.card.name}.${action}`);



    let promisedTask = (task) => {

      let RelayBaton = {};

      let taskPromise = new Promise((resolve, reject) => {

        let taskPath = './' + task.id + '/index.es6';
        // console.log('EXECUTING [[%s]]: %s', task.id, taskPath);
        //console.log('EXECUTING [[%s]]: %s', task.id, taskPath);



        try {

          let resolver = {};

          resolver.resolve = function(){

            //console.log(`Task ${task.id} has been resolved.`);
            //console.log(`Task ${task.id} has been resolved.`);
            resolve();

          };

          resolver.reject = function(){

            //console.log(`Task ${task.id} has been rejected.`);
            //console.log(`Task ${task.id} has been rejected.`);
            reject();

          };

          // prepare
          extend( true, this.data, task, resolver, {taskPath}, {info:this.info, card: this.card, flow: this} );

          // executable action
          let taskModule = this.code( taskPath );

          taskModule(this.data);
          //console.log('EXECUTED [[%s]]: %s', task.id, taskModule,this.data);

        } catch (e) {

          console.log('ERROR WHILE EXECUTING [[%s]]', task.id);
          console.log('ACTION ERROR', e);
          console.log('ACTION SOURCECODE IS LOCATED AT:', taskPath);
          console.dir( this.code.keys() );

          // Note that while the error is logged, the program continues on.
          resolve();
        }

      });

      return taskPromise;

    };


    //console.log(resolved)

    Promise.each(resolved, promisedTask).done(() => {

     // console.log("Action FINISHED: Event Name %s.", action);
      //console.log("Action FINISHED: Event Name %s.", action);

      this.widget._trigger( "complete", null, { action } );

    });


  }

}

class Factory {

  constructor({widget, info, card, flow, code } = {}) {

    this.widget = widget;
    this.info = info;
    this.card = card;
    this.flow = flow;
    this.code = code;

   }

  getControl({}) {
    return new Control({ widget: this.widget, info: this.info, card: this.card, flow: this.flow, code: this.code });
  }

}

// By design called from browser, inside jQuery-ui widget.
module.exports = Factory;
